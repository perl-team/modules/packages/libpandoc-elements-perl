libpandoc-elements-perl (0.38-7) unstable; urgency=medium

  [ Debian Janitor ]
  * update lintian overrides

  [ Jonas Smedegaard ]
  * declare compliance with Debian Policy 4.7.0

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 03 Aug 2024 11:47:42 +0200

libpandoc-elements-perl (0.38-6) unstable; urgency=medium

  * tighten (build-)dependency on libpandoc-wrapper-perl,
    to ensure recent pandoc is supported
  * update copyright info:
    + use field Reference (not License-Reference)
    + update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 02 Jan 2023 22:35:32 +0100

libpandoc-elements-perl (0.38-5) unstable; urgency=medium

  * add patches cherry-picked upstream:
    + Make sure DefinitionPair has a TO:JSON method
    + Add more recent Pandoc versions
  * update patch 1001 to cover currently packaged Pandoc
  * declare compliance with Debian Policy 4.6.2
  * update git-buildpackage config:
    + use DEP-14 git branches
    + enable automatic DEP-14 branch name handling
    + add usage config
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 26 Dec 2022 11:05:01 +0100

libpandoc-elements-perl (0.38-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on libpandoc-wrapper-perl.
    + libpandoc-elements-perl: Drop versioned constraint on
      libpandoc-wrapper-perl in Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 04 Jul 2022 18:27:07 +0100

libpandoc-elements-perl (0.38-3) unstable; urgency=medium

  * add patch 1001 to cover more recent releases of Pandoc;
    closes: bug#966934, thanks to Lucas Nussbaum
  * declare compliance with Debian Policy 4.6.0
  * stop build-depend explicitly on perl: not called directly

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 13 Sep 2021 05:08:04 +0200

libpandoc-elements-perl (0.38-2) unstable; urgency=medium

  [ Debian Janitor ]
  * build-depend on debhelper-compat (not debhelper)
  * set upstream metadata fields:
    Bug-Database Bug-Submit Repository Repository-Browse

  [ Jonas Smedegaard ]
  * add patch 1001 to modernize API coverage;
    closes: bug#966934, thanks to Lucas Nussbaum
  * declare compliance with Debian Policy 4.5.0
  * use debhelper compatibility level 13 (not 12)
  * watch: rewrite usage comment
  * simplify source script copyright-chceck
  * copyright: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 21 Aug 2020 13:29:55 +0200

libpandoc-elements-perl (0.38-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).

  [ gregor herrmann ]
  * debian/*: update URLs from {search,www}.cpan.org to MetaCPAN.
  * debian/*: update GitHub URLs to use HTTPS.

  [ Laurent Baillet ]
  * fix lintian wrong-path-for-interpreter error

  [ Jonas Smedegaard ]
  * Simplift rules: Use relative path.
  * Update watch file: Fix typo in usage comment.
  * Update copyright info: Extend coverage for main upstream author.
  * Declare compliance with Debian Policy 4.2.1.
  * Wrap and sort control file.
  * Tighten (build-)dependency on libpandoc-wrapper-perl.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 04 Nov 2018 05:09:20 +0100

libpandoc-elements-perl (0.36-2) unstable; urgency=medium

  * Team upload.
  * Make the debhelper build dependency versioned
  * Update to debhelper compat level 10
  * Declare that the package does not need (fake)root to build
  * Add Testsuite declaration for autopkgtest-pkg-perl

 -- Niko Tyni <ntyni@debian.org>  Sun, 06 May 2018 19:05:24 +0300

libpandoc-elements-perl (0.36-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).
    0.34:
    + Extend metadata value method with JSON Pointer and options.
    + Change default stringification of MetaBlocks.
    + Fixes provided by Benct Philip Jonsson and TakeAsh.
    + Remove internal module Pandoc::Filter::Usage.
    0.35:
    + Fix test failures.
    + Fix, extend and document ImageFromCode filter.
    0.36:
    + Extend ImageFromCode filter.
    + Introduce Pandoc::Selector by refactoring.
    + Add select filter.
    + Remove deprecated function stringify.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Jonas Smedegaard ]
  * Update package relations:
    + Skip testsuite-related build-dependencies with hint !nocheck (not
      deprecated !stage1).
    Closes: Bug#892761. Thanks to Helmut Grohne.
    + Build-depend on libtest-warnings-perl.
    + Tighten to (build-)depend versioned on libpandoc-wrapper-perl.
  * Declare compliance with Debian Policy 4.1.4.
  * Update watch file: Use substitution variables.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 30 Apr 2018 12:23:32 +0200

libpandoc-elements-perl (0.33-2) unstable; urgency=medium

  * Modernize Vcs-Browser field: Use git (not cgit) in path.
  * Declare compliance with Debian Policy 4.1.0.
  * Update copyright info: Use https protocol in file format URL.
  * Tighten lintian overrides regarding License-Reference.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 28 Aug 2017 19:52:28 +0200

libpandoc-elements-perl (0.33-1) experimental; urgency=medium

  [ upstream ]
  * New release(s).

  [ Jonas Smedegaard ]
  * Mention inclusion of pandoc filter multifilter in long description.
  * Refactor to limit use of CDBS.
  * Update package relations:
    + (Build-)depend on libpandoc-wrapper-perl.
    + Build-depend on libtest-deep-perl.
    + Stop build-depend on cdbs or licensecheck or dh-buildinfo.
    + Stop recommend pandoc.
    + Skip testsuite build-dependencies for stage1 builds.
  * Avoid install empty manpage.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 31 Mar 2017 13:12:06 +0200

libpandoc-elements-perl (0.24-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).
    + Support new (Pandoc 1.16) document model with Link and Image attributes.
    + Fix blessing metadata top level values.
    + Support stringification of meta elements.
    + Add SoftBreak inline element, introduced in Pandoc 1.16.
    + Fix and extend attributes function.
    + Document and test removal of SoftBreak element.
    + fix json serialization to ensure right scalar types.
    + Extend attributes with Hash::MultiValue.
    + Drop support of special key 'classes' in attributes helper function.
    + Add graphviz example.
    + extend method 'keyvals' as setter.
    + document and extend metadata elements by method 'metavalue'.
    + let to_json return canonical JSON.
    + Add multifilter script to run filters specified in document metadata.
    + Add metavars example.
    + Make most getters also setters.

  * Update watch file:
    + Bump to file format 4.
    + Watch only MetaCPAN URL.
    + Add usage comment.
    + Tighten version regex.
  * Drop CDBS get-orig-source target: Use gbp import-orig --uscan.
  * Modernize git-buildpackage config: Filter any .git* file.
  * Declare compliance with Debian Policy 3.9.8.
  * Modernize Vcs-Git field: Use https protocol.
  * Update copyright info: Extend coverage of Debian packaging.
  * Update package relations:
    + (Build-)depend on libhash-multivalue-perl libipc-run3-perl.
    + Build-depend on libtest-exception-perl.
    + Build-depend on licensecheck (not devscripts).
  * Drop obsolete lintian override regarding debhelper 9.
  * Install examples.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 10 Jan 2017 21:54:48 +0100

libpandoc-elements-perl (0.15-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).
    + Add pod2pandoc script and Pod::Simple::Pandoc.
    + Add string as method to replace function stringify.
    + Fix nasty bug in Pandoc::Element constructor reuse.
    + Fix passing of output format.
    + Add method match (experimental).
    + Add examples myemph.pl and theorem.pl.
    + Refactor and introduce Pandoc::Filter::Lazy.
    + Fix Pod::Simple::Pandoc for old Pod::Simple.
    + Allow more lazy filter scripting.
    + Add example to remove unnumbered sections.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 28 Dec 2015 01:03:10 +0530

libpandoc-elements-perl (0.11-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).
    + Support definition of filters as hash.
    + Add utility script pandoc-walk.
    + New method: pandoc_walk.
    + Provide special variable $_ to action functions.
    + UTF-8 output with pandoc_walk and pandoc_filter.
    + Include tests of pandoc-walk.

  [ Jonas Smedegaard ]
  * Update copyright info: Tighten licensing of packaging to GPL-3+.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 08 Dec 2015 01:21:37 +0530

libpandoc-elements-perl (0.08-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).
    + Extend documentation and examples.
    + New helper function: citation.

  [ Jonas Smedegaard ]
  * Update copyright info:
    + Extend coverage of packaging.
    + Relax packaging license to GPL-2+.
  * Fix lintian overrides.
  * Bump debhelper compatibility level to 9.
  * Add lintian override regarding debhelper 9.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 24 Oct 2015 19:43:27 +0200

libpandoc-elements-perl (0.06-1) unstable; urgency=low

  * Initial Release.
    Closes: bug#781000.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 01 Jun 2015 19:52:47 +0200
